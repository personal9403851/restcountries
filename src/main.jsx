import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import RestContextProvider from "./context/RestContextProvider.jsx";
import {
  Route,
  RouterProvider,
  createBrowserRouter,
  createRoutesFromElements,
} from "react-router-dom";
import MainContainer from "./components/MainContainer/MainContainer.jsx";
import SpecificCountry from "./components/SpecificCountry/SpecificCountry.jsx";

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route path="/" element={<App />}>
      <Route path="/" element={<MainContainer />} />
      <Route path="country/:id" element={<SpecificCountry />}></Route>
    </Route>
  )
);

ReactDOM.createRoot(document.getElementById("root")).render(
  <RestContextProvider>
    <RouterProvider router={router} />
  </RestContextProvider>
);
