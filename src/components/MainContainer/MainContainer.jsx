import Cards from "../Cards/Cards";
import Search from "../Search/Search";
import Filter from "../Filter/Filter";
import Sort from "../Sort/Sort";
import FilterSubRegion from "../FilterSubRegion/FilterSubRegion";
import RestContext from "../../context/RestContext";
import { useContext } from "react";

function MainContainer() {
  const { darkModeColor } = useContext(RestContext);
  return (
    <div
      className={`main-container ${
        darkModeColor ? "dark-background" : "light"
      }`}
    >
      <div className="search-and-filter">
        <Search />
        <Sort />
        <FilterSubRegion />
        <Filter />
      </div>
      <Cards />
    </div>
  );
}

export default MainContainer;
