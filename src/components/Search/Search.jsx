import { useContext } from "react";
import RestContext from "../../context/RestContext";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";

function Search() {
  const { setSearchInput, searchInput, darkModeColor } =
    useContext(RestContext);

  return (
    <div
      className={`search ${darkModeColor ? "dark-elements" : "search-light"}`}
    >
      <FontAwesomeIcon
        icon={faMagnifyingGlass}
        className={`search-icon ${
          darkModeColor ? "search-dark" : "search-light"
        }`}
      />
      <input
        type="search"
        className={`search-box ${
          darkModeColor ? "dark-elements" : "search-light"
        }`}
        placeholder="Search for a country..."
        value={searchInput}
        onChange={(e) => {
          setSearchInput(e.target.value);
        }}
      />
    </div>
  );
}

export default Search;
