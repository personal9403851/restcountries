import { useContext, useEffect } from "react";
import { Link } from "react-router-dom";
import RestContext from "../../context/RestContext";

function Cards() {
  const {
    darkModeColor,
    countriesAll,
    setCountriesAll,
    subRegion,
    searchInput,
    filterValue,
    sortValue,
    isArea,
    setIsArea,
    loading,
    setLoading
  } = useContext(RestContext);

  useEffect(() => {
    getCountryData();
  }, []);

  const getCountryData = () => {
    fetch("https://restcountries.com/v3.1/all")
      .then((res) => {
        if (res.ok) return res.json();
      })
      .then((data) => {
        setCountriesAll(data);
        setLoading(false);
      })
      .catch((error) => console.log(error));
  };

  let filteredCountries = countriesAll.filter((country) => {
    if (filterValue != "") {
      if (subRegion != "") {
        if (
          country.subregion === subRegion &&
          country.name.common.toLowerCase().includes(searchInput.toLowerCase())
        )
          return true;
        else return false;
      } else {
        if (
          country.name.common
            .toLowerCase()
            .includes(searchInput.toLowerCase()) &&
          country.region.toLowerCase() == filterValue.toLowerCase()
        ) {
          return true;
        } else return false;
      }
    } else {
      if (
        country.name.common.toLowerCase().includes(searchInput.toLowerCase())
      ) {
        return true;
      }
      return false;
    }
  });

  if (sortValue != "") {
    if (sortValue == "population-ascending") {
      setIsArea(false);
      filteredCountries = filteredCountries.sort((countryA, countryB) => {
        return countryA.population - countryB.population;
      });
    } else if (sortValue == "population-descending") {
      setIsArea(false);
      filteredCountries = filteredCountries.sort((countryA, countryB) => {
        return countryB.population - countryA.population;
      });
    } else if (sortValue == "area-ascending") {
      setIsArea(true);
      filteredCountries = filteredCountries.sort((countryA, countryB) => {
        return countryA.area - countryB.area;
      });
    } else if (sortValue == "area-descending") {
      setIsArea(true);
      filteredCountries = filteredCountries.sort((countryA, countryB) => {
        return countryB.area - countryA.area;
      });
    } else {
    }
  }

  if(loading){
    return <h2 style={{textAlign:"center"}}>Loading....</h2>
  }

  return (
    <div className="cards-container">
   
      {filteredCountries.length != 0 ? (
        filteredCountries.map((eachCountry) => {
          return (
            <Link to={`/country/${eachCountry.cca3}`}>
              <div
                className={`card ${
                  darkModeColor ? "dark-elements" : "light-elements"
                }`}
                key={eachCountry.cca3}
              >
                <img
                  src={eachCountry.flags.png}
                  alt="Unable to load image"
                  className="country-image"
                />
                <div className="country-info">
                  <p className="name">{eachCountry.name.common}</p>
                  <p className="population">
                    Population:{" "}
                    <span className="light-size">{eachCountry.population}</span>
                  </p>
                  <p className="region">
                    Region:{" "}
                    <span className="light-size">{eachCountry.region}</span>
                  </p>
                  <p className="capital">
                    Capital:{" "}
                    {eachCountry.capital ? (
                      <span className="light-size">
                        {eachCountry.capital[0]}
                      </span>
                    ) : (
                      ""
                    )}
                  </p>
                  {isArea ? (
                    <p
                      className={`area ${
                        isArea ? "area-display-block" : "area-display-none"
                      }`}
                    >
                      Area:{" "}
                      <span className="light-size">{eachCountry.area}</span>
                    </p>
                  ) : (
                    ""
                  )}
                </div>
              </div>
            </Link>
          );
        })
      ) : (
        <div className="country-not-found">No such countries found</div>
      )}
    </div>
  );
}
export default Cards;
