import React, { useContext, useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom";
import RestContext from "../../context/RestContext";

function SpecificCountry() {
  const [specificCountryData, setSpecificCountryData] = useState("");
  const { darkModeColor, loading, setLoading } = useContext(RestContext);
  const { id } = useParams();

  useEffect(() => {
    fetchSpecificCountryData();
  }, [id]);

  const fetchSpecificCountryData = () => {
    fetch(`https://restcountries.com/v3.1/alpha?codes=${id}`)
      .then((res) => {
        if (res.ok) {
          return res.json();
        }
      })
      .then((data) => {
          setLoading(false);
        setSpecificCountryData(data);
      })
      .catch((error) => console.log(error));
  };


  if(loading){
    return <h2 style={{textAlign:"center"}}>Loading....</h2>
  }

  return (
    <div
      className={`specific-country-div ${
        darkModeColor ? "specific-dark-background" : "light-elements"
      }`}
    >
      <Link to={`/`}>
        <button
          className={`border-buttons ${
            darkModeColor ? "specific-dark-elements" : "light-elements"
          }`}
        >
          &larr;{"  "}Back
        </button>
      </Link>
      
      {specificCountryData[0] ? (
        <div className="specific-country-details">
          <div className="specific-country-image">
            <img
              src={specificCountryData[0].flags.png}
              alt="Unable to load image"
            />
          </div>
          <div className="specific-country-info">
            <p className="specific-country-name">
              {specificCountryData[0].name.common}
            </p>
            <div className="specific-country-data">
              <div className="left-data">
                <p className="specific-property">
                  Native Name:{" "}
                  <span className="light-size">
                    {specificCountryData[0].name.nativeName
                      ? specificCountryData[0].name.nativeName.eng
                        ? specificCountryData[0].name.nativeName.eng.common
                        : Object.values(
                            specificCountryData[0].name.nativeName
                          ).slice(-1)[0].common
                      : "No native name"}
                  </span>
                </p>
                <p className="specific-property">
                  Population:{"  "}
                  <span className="light-size">
                    {specificCountryData[0].population
                      ? specificCountryData[0].population
                      : null}
                  </span>
                </p>
                <p className="specific-property">
                  Region:{" "}
                  <span className="light-size">
                    {specificCountryData[0].region
                      ? specificCountryData[0].region
                      : null}
                  </span>
                </p>
                <p className="specific-property">
                  Sub Region:{" "}
                  <span className="light-size">
                    {specificCountryData[0].subregion
                      ? specificCountryData[0].subregion
                      : null}
                  </span>
                </p>
                <p className="specific-property">
                  Capital:{" "}
                  <span className="light-size">
                    {specificCountryData[0].capital
                      ? specificCountryData[0].capital
                      : null}
                  </span>
                </p>
              </div>
              <div className="right-data">
                <p className="specific-property">
                  Top Level Domain:{" "}
                  <span className="light-size">
                    {specificCountryData[0].tld[0]
                      ? specificCountryData[0].tld[0]
                      : null}
                  </span>
                </p>
                <p className="specific-property">
                  Currencies:{" "}
                  <span className="light-size">
                    {specificCountryData[0].currencies
                      ? Object.keys(specificCountryData[0].currencies)[0]
                      : null}
                  </span>
                </p>
                <p className="specific-property">
                  Languages:{" "}
                  <span className="light-size">
                    {specificCountryData[0].languages
                      ? Object.values(specificCountryData[0].languages).join(
                          ", "
                        )
                      : null}
                  </span>
                </p>
              </div>
            </div>
            <div className="border-countries">
              Border Countries:{" "}
              {specificCountryData[0].borders
                ? specificCountryData[0].borders.map((eachBorder) => {
                    console.log(eachBorder);
                    return (
                      <Link to={`/country/${eachBorder}`}>
                        <button
                          className={`border-buttons ${
                            darkModeColor
                              ? "specific-dark-elements"
                              : "light-elements"
                          }`}
                        >
                          {eachBorder}
                        </button>
                      </Link>
                    );
                  })
                : null}
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
}

export default SpecificCountry;
