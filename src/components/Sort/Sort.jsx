import RestContext from "../../context/RestContext";
import { useContext, useState } from "react";

function Filter() {
  const { sortValue, setSortValue, darkModeColor } = useContext(RestContext);

  return (
    <select
      className={`filter-region ${
        darkModeColor ? "dark-elements" : "search-light"
      }`}
      onChange={(e) => {
        setSortValue(e.target.value);
      }}
      value={sortValue}
    >
      <option value="" hidden>
        Sort By
      </option>
      <option value="population-ascending">Population(Asc)</option>
      <option value="population-descending">Population(Desc)</option>
      <option value="area-ascending">Area(Asc)</option>
      <option value="area-descending">Area(Desc)</option>
    </select>
  );
}

export default Filter;
