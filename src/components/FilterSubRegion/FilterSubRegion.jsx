import RestContext from "../../context/RestContext";
import { useContext, useState } from "react";

function Filter() {
  const { darkModeColor, countriesAll, setSubRegion, filterValue, setSortValue } = useContext(RestContext);
  const [selectedOption, setSelectedOption] = useState("");

  let index = 0;

  const subRegionName = [];
  countriesAll.forEach((country) => {
    if (
      !subRegionName.includes(country.subregion) &&
      filterValue === country.region
    )
      subRegionName.push(country.subregion);
  });

  return (
    <select
      className={`filter-region ${
        darkModeColor ? "dark-elements" : "search-light"
      }`}
      onChange={(e) => {
        setSubRegion(e.target.value);
        setSelectedOption(e.target.value);
        setSortValue("");
      }}
      value={selectedOption}
    >
      <option value="" hidden>
        Filter By Subregion
      </option>

      {subRegionName.map((eachSubRegion) => {
        index += 1;
        return (
          <option value={eachSubRegion} key={index}>
            {eachSubRegion}
          </option>
        );
      })}
    </select>
  );
}

export default Filter;
