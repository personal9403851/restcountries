import RestContext from "../../context/RestContext";
import { useContext } from "react";

function Filter() {
  const { setFilterValue, darkModeColor, countriesAll, setSubRegion, setSortValue } =
    useContext(RestContext);
  
  let index = 0;

  const regionName = [];
  countriesAll.forEach((country) => {
    if (!regionName.includes(country.region)) regionName.push(country.region);
  });

  return (
    <select
      className={`filter-region ${
        darkModeColor ? "dark-elements" : "search-light"
      }`}
      onChange={(e) => {
        setFilterValue(e.target.value);
        setSubRegion("");
        setSortValue("");
      }}
    >
      <option value="filter-by-region" hidden>
        Filter By Region
      </option>

      {regionName.map((eachRegion) => {
        index += 1;
        return (
          <option value={eachRegion} key={index}>
            {eachRegion}
          </option>
        );
      })}
    </select>
  );
}

export default Filter;
