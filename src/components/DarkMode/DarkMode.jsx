import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMoon } from "@fortawesome/free-regular-svg-icons";
import RestContext from "../../context/RestContext";
import { useContext } from "react";

function DarkMode() {

  const {setDarkModeColor} = useContext(RestContext)
  return (
    <div className="dark-mode" onClick={() => setDarkModeColor((prev) => !prev)}>
      <FontAwesomeIcon icon={faMoon} />
      <p className="name-of-mode">Dark Mode</p>
    </div>
  );
}

export default DarkMode;
