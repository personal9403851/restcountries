import RestContext from "./RestContext";
import { useState } from "react";

const RestContextProvider = ({ children }) => {
  const [searchInput, setSearchInput] = useState("");
  const [filterValue, setFilterValue] = useState("");
  const [darkModeColor, setDarkModeColor] = useState(false);
  const [countriesAll, setCountriesAll] = useState([]);
  const [subRegion, setSubRegion] = useState("");
  const [sortValue, setSortValue] = useState("");
  const [isArea, setIsArea] = useState(false);
  const [loading, setLoading] = useState(true);

  return (
    <RestContext.Provider
      value={{
        searchInput,
        setSearchInput,
        filterValue,
        setFilterValue,
        darkModeColor,
        setDarkModeColor,
        countriesAll,
        setCountriesAll,
        subRegion,
        setSubRegion,
        sortValue,
        setSortValue,
        isArea,
        setIsArea,
        loading,
        setLoading
      }}
    >
      {children}
    </RestContext.Provider>
  );
};

export default RestContextProvider;
